from appium.webdriver.common.multi_action import MultiAction
from selenium.webdriver.common.by import By
from selenium.webdriver.support import wait, expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from appium import webdriver
from src.helpers.appiumdriver import Driver
from selenium.common.exceptions import NoSuchElementException
import time
import logging
from enum import Enum
from selenium import webdriver
from appium.webdriver.common.touch_action import TouchAction

class App(Driver):

    def __init__(self, driver):
        super().__init__(driver)

    def open_url(self, URL):
        return self.driver.get(URL)

    def swipe_down(self):
        self.driver.execute_script("mobile: swipe", {"direction": "down"})

    def swipe_new_left(self):
        self.driver.execute_script("mobile: swipe", {"direction": "left"})

    def swipe_new_right(self):
        self.driver.execute_script("mobile: swipe", {"direction": "right"})

    def swipe_up(self):
        self.driver.execute_script("mobile: swipe", {"direction": "up"})

    def wait_for_element(self):
        if(wait == None):
            wait.WebDriverWait(self.driver, 25)

        else:
         return wait

    class Direction(Enum):
        DOWN = 1
        UP = 2
        LEFT=3
        RIGHT=4

    def swipe_right(self, durationz):
        sizze = self.driver.get_window_size()
        startX = 0
        endX = 0
        startY = 0
        endY = 0
#        startY = sizze.height / 2
#        startX = sizze.width * 0.90
#        endX = sizze.width * 0.05
        actions = TouchAction(self.driver)
        actions.tap(startX, startY)
        actions.wait(self,durationz)
        actions.move_to(endX, startY)
        actions.release()
        actions.perform()

    def swipe_left(self):
        logging.info('Calculating Size')
        size = self.driver.get_window_size()
        logging.info(self.driver.get_window_size())
        width = self.driver.get_window_size()['width']
        height = self.driver.get_window_size()['height']
        logging.info(width)
        logging.info(height)
        istartX = 0
        logging.info(istartX)
        iendX = 0
        logging.info(iendX)
        istartY = 0
        logging.info(istartY)
        iendY = 0
        logging.info(iendY)
        # startY = int(round(height / 2))
        # logging.info(startY)
        # logging.info(int(round(startY)))
        # istartX = int(round(width * 0.05))
        # logging.info(int(round(istartX)))
        # iendX = int(int(round(width * 0.90)))
        # logging.info(int(round(iendX)))

        actions = TouchAction(self.driver)
        actions.tap(istartX, istartY)
        actions.move_to(iendX, istartY)
        actions.release()
        actions.perform()



    def main_menu_items_test(self):
        logging.info('\t TestCase 7 : Started  \n')
        time.sleep(5)
        logging.info('\t TestCase 7 : Clicking the Menu  \n')
        time.sleep(3)
        menuItem = self.driver.find_element_by_class_name("hamburger-icon")
        menuItem.click()
        logging.info('\t TestCase 7 : Menu is clicked  \n')
        for i in range(1,11,1):
            logging.info("TestCase 7 : Testing Menu : " +str(i))
            time.sleep(2)
            menuListItem = self.driver.find_element_by_css_selector("div.z-header > nav  li:nth-child("+ str(i) +") > a > i")
            logging.info('TestCase 7 : Testing Menu : '+str(i) +'licking on Expandable icon to open it')
            menuListItem.click() #Open
            time.sleep(2)
            logging.info('TestCase 7 : Testing Menu : '+ str(i) + 'licking on Expandable icon to close it')
            menuListItem.click() #close

    def swipe_breadcrumb(self):
        header=self.driver.find_element_by_css_selector("body > div.alert.global-alert > a")
        price=self.driver.find_element_by_css_selector(".product-price__price")
        App.swipe_to_element_view(self,price)
        time.sleep(3)
        App.swipe_to_element_view(self, header)
        time.sleep(3)
        logging.info('\t Test-5 Test Started : \n')
        logging.info('Waiting for 5 sec')
        time.sleep(5)
        logging.info('Swiping Start')
        for x in range(3):
            App.swipe_new_right(self)
            time.sleep(3)
        for x in range(3):
            App.swipe_new_left(self)
            time.sleep(3)

    def image_swipe(self):
        logging.info('\t TestCase 4 : Started :  \n')
        image = self.driver.find_element_by_css_selector(".slick-active > div > div > img")
        image.click()

        logging.info('Waiting for 3 sec')
        time.sleep(3)
        logging.info('Swiping Start')
        for x in range(10):
            App.swipe_new_left(self)
            time.sleep(1)

        for y in range(10):
            App.swipe_new_right(self)
            time.sleep(1)

        logging.info('TestCase 4 : Ended :')

    def swipe_to_element_view(self, element):
        self.driver.execute_script("arguments[0].scrollIntoView();", element)

    def product_swipe(self):
        logging.info('\t TestCase 3 : Started :  \n')
        App.swipe_up(self)
        App.swipe_up(self)
        time.sleep(5)
        element=self.driver.find_element_by_css_selector("section:nth-child(2) h2")
        App.swipe_to_element_view(self,element)
        for x in range(20):
            App.swipe_new_left(self)
            time.sleep(1)
        for y in range(20):
            App.swipe_new_right(self)
            time.sleep(1)
        logging.info('TestCase 3 : Ended :')

    def scroll_up_and_down(self):
        logging.info('TestCase 2 : Started :')
        footer=self.driver.find_element_by_css_selector("div.lastFoot")
        header=self.driver.find_element_by_css_selector("a.logo-container > img")

        for x in range(13):
              App.swipe_up(self)

        for y in range(13):
              App.swipe_down(self)

        logging.info('TestCase 2 : Ended :')

    def click_on_product_image(self):
        logging.info('TestCase 1 : Started :')
        product_image=self.driver.find_element_by_css_selector(".slick-active > div > div > img")
        product_image.click()
        time.sleep(5)

    def zoom_in_zoom_out(self):
        # Zoom In State
        a1 = TouchAction()
        a1.press(None, 70, 200)
        a1.wait(ms=1000)
        a1.move_to(None, 96, 156)
        a1.release()
        a2 = TouchAction()
        a2.press(None, 96, 156)
        a2.wait(ms=1000)
        a2.move_to(None, 70, 200)
        a2.release()
        ma = MultiAction(self.driver)
        ma.add(a1, a2)
        ma.perform()
        time.sleep(5)

        # Zoom Out
        a1 = TouchAction()
        a1.press(None, 70, 200)
        a1.wait(ms=2000)
        a1.move_to(None, 96, 156)
        a1.release()
        a2 = TouchAction()
        a2.press(None, 96, 156)
        a2.wait(ms=2000)
        a2.move_to(None, 70, 200)
        a2.release()
        ma = MultiAction(self.driver)
        ma.add(a1, a2)
        ma.perform()
        time.sleep(5)

        # Move while App is in Zoom in state
        a1 = TouchAction()
        a1.press(None, 70, 200)
        a1.wait(ms=2000)
        a1.move_to(None, 96, 156)
        a1.release()
        ma = MultiAction(self.driver)
        ma.add(a1)
        ma.perform()
        time.sleep(5)

    def element(self, locator, n=5):
        """
        locate an element by polling if element not found
        maximum poll #4 with approx. ~20 secs
        """
        while n > 1:
            try:
                return self.driver.find_element(*locator)
            except Exception as e:
                n -= 1
                if n == 1: raise NoSuchElementException("Could not locate element with value: %s" % str(locator))

    def elements(self, locator):
        return self.driver.find_elements(*locator)

    def assert_text(self, locator, text, n=5):
        """
        assert element's text by polling if match is not found
        maximum poll #4 with approx. ~20 secs
        """
        while n > 1:
            try:
                assert App.element(self, locator).text == text
                break
            except Exception as e:
                time.sleep(5)
                n -= 1
                if n == 1: assert App.element(self, locator).text == text
