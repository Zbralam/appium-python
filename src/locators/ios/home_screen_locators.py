from appium.webdriver.common.mobileby import MobileBy


class HomeLocators(object):
    """
    home screen locators
    """
    loginMenu = (MobileBy.XPATH, "//ios.view.ViewGroup[@content-desc='Login']/ios.widget.TextView")
    formsMenu = (MobileBy.ACCESSIBILITY_ID, "Forms")
    homeMenu = (MobileBy.ACCESSIBILITY_ID, "Home")
    swipeMenu = (MobileBy.ACCESSIBILITY_ID, "Swipe")

    # swipeMenu = {'ANDROID': (MobileBy.ACCESSIBILITY_ID, "Swipe"),'IOS': (MobileBy.ACCESSIBILITY_ID, "Swipe")}