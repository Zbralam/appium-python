from appium.webdriver.common.mobileby import MobileBy


class LoginLocators(object):
    """
    login screen locators
    """
    inputField = (MobileBy.XPATH, '//ios.widget.EditText[@content-desc="input-email"]')
    passwordField = (MobileBy.ACCESSIBILITY_ID, 'input-password')
    loginButton = (MobileBy.XPATH, '//ios.view.ViewGroup[@content-desc="button-LOGIN"]/ios.view.ViewGroup')