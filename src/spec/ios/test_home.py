import time

from src.helpers.appiumdriver import Driver
from src.helpers.app import App
import logging


class HomeTest(Driver):

    def __init__(self, driver):
        super().__init__(driver)


    def test_main_menu_test(self):
        URL = "https://www.zoro.com"
        logging.critical('Begin: main_menu_test() - Zoro Mobile Test \n')
        logging.critical('\t Open Safari Browser in simulator \n')
        logging.critical('\t Browse to a mobile website \n')
        logging.critical('\t Test-7 Verify that evert category is expandable \n')
        logging.critical('\t Open Chrome, browse to: \n')
        App.open_url(self, URL)
        time.sleep(2)
        logging.critical('\t Test-7 Verify that evert category is expandable\n')
        App.main_menu_items_test(self)

    def test_breadcrumb(self):
       URL = "https://www.zoro.com/dewalt-string-trimmer-line-0080in-dia-225-ft-dwo1dt802/i/G9981745/"
       logging.critical('Begin: breadcrumb_test() - Zoro Mobile Test \n')
       logging.critical('\t Open Safari Browser in simulator \n')
       logging.critical('\t Browse to a mobile website \n')
       logging.critical('\t Test-5 Swipe on Breadcrumb \n')
       logging.critical('\t Open Chrome, browse to: \n')
       App.open_url(self, URL)
       time.sleep(5)
       App.swipe_breadcrumb(self)

    def test_more_image_swipe(self):
        URL = "https://www.zoro.com/werner-extension-ladder-fiberglass-24-ft-ia-d6224-2/i/G2739292/"
        logging.critical('Begin: more_images_swipe() - Zoro Mobile Test \n')
        logging.critical('\t Open Safari Browser in simulator \n')
        logging.critical('\t Browse to a mobile website \n')
        logging.critical('\t Test-4 Swipe the current product picture from left to right \n')
        logging.critical('\t Open Chrome, browse to: \n')
        App.open_url(self, URL)
        time.sleep(2)
        logging.critical('\t Test-4 Swipe the current product picture from left to right \n')
        App.image_swipe(self)


    def test_product_swipe(self):
        URL = "https://www.zoro.com/werner-extension-ladder-fiberglass-24-ft-ia-d6224-2/i/G2739292/"
        logging.critical('Begin: product_swipe() - Zoro Mobile Test \n')
        logging.critical('\t Open Safari Browser in simulator \n')
        logging.critical('\t Browse to a mobile website \n')
        logging.critical('\t Test-3 Swipe You may like Products until the end of the carousel \n')
        logging.critical('\t Open Chrome, browse to: \n')
        App.open_url(self, URL)
        time.sleep(2)
        logging.critical('\t Test-3 Swipe You may like Products until the end of the carousel \n')
        App.product_swipe(self)


    def test_scroll_up_and_down(self):
        URL = "https://www.zoro.com/werner-extension-ladder-fiberglass-24-ft-ia-d6224-2/i/G2739292/"
        logging.critical('Begin: scroll_up_and_down() - Zoro Mobile Test \n')
        logging.critical('\t Open Safari Browser in simulator \n')
        logging.critical('\t Browse to a mobile website \n')
        logging.critical('\t Test-2 Scroll the Page to Top to Bottom and vice versa \n')
        logging.critical('\t Open Chrome, browse to: \n')
        App.open_url(self, URL)
        time.sleep(2)
        logging.critical('\t Test-2 Scroll the Page to Top to Bottom and vice versa \n')
        App.scroll_up_and_down(self)

    def test_zoom_in_zoom_out(self):
        URL = "https://www.zoro.com/werner-extension-ladder-fiberglass-24-ft-ia-d6224-2/i/G2739292/"
        logging.critical('Begin: zoom_in_zoom_out() - Zoro Mobile Test \n')
        logging.critical('\t Open Safari Browser in simulator \n')
        logging.critical('\t Browse to a mobile website \n')
        logging.critical('\t Test-1 Zoom in and out on Picture \n')
        logging.critical('\t Open Chrome, browse to: \n')
        App.open_url(self, URL)
        time.sleep(2)
        logging.critical('\t Test-1 Zoom in and out on Picture \n')
        App.click_on_product_image(self)
        App.zoom_in_zoom_out(self)
        logging.info('TestCase 1 : Ended :')

    def test_main_page_zoom_in_zoom_out(self):
        URL = "https://www.zoro.com"
        logging.critical('Begin: zoom_in_zoom_out() - Zoro Mobile Test \n')
        logging.critical('\t Open Safari Browser in simulator \n')
        logging.critical('\t Browse to a mobile website \n')
        logging.critical('\t Test-6 Zoom in and out on Picture \n')
        logging.critical('\t Open Chrome, browse to: \n')
        App.open_url(self, URL)
        time.sleep(2)
        logging.critical('\t Test-6 Zoom in and out on Main Page \n')
        App.zoom_in_zoom_out(self)
        logging.info('TestCase 6 : Ended :')

